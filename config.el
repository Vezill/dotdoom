;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;;; Doom-wide

(setq doom-theme 'doom-monokai-pro
      doom-themes-enable-bold t
      doom-themes-enable-italic t
      display-line-numbers-type 'relative
      company-idle-delay nil            ; disable automatic auto-complete popup

      ;; doom-font (font-spec :family "iosevka" :weight 'regular :size 16)
      ;; doom-big-font (font-spec :family "iosevka" :weight 'regular :size 28))
      ;; doom-font (font-spec :family "Fira Code Retina" :weight 'regular :size 15)
      ;; doom-big-font (font-spec :family "Fira Code Retina" :weight 'regular :size 26))
      ;; doom-font (font-spec :family "MonoLisa" :weight 'regular :size 14)
      ;; doom-big-font (font-spec :family "MonoLisa" :weight 'regular :size 24))
      doom-font (font-spec :family "JetBrains Mono" :weight 'regular :size 14)
      doom-big-font (font-spec :family "JetBrains Mono" :weight 'regular :size 20))

(setq-default fill-column 120
              buffer-file-coding-system 'utf-8-unix
              default-buffer-file-coding-system 'utf-8-unix)

;; (doom/set-frame-opacity 95)

;;; Language Indentation

(setq-hook! 'web-mode-hook
  web-mode-attr-indent-offset 2
  web-mode-attr-value-indent-offset 2
  web-mode-css-indent-offset 2
  web-mode-code-indent-offset 2
  web-mode-markup-indent-offset 2
  web-mode-sql-indent-offset 2)

(setq-hook! 'js2-mode-hook
  js2-basic-offset 2)

(setq-hook! 'typescript-mode-hook
  typescript-indent-level 2)

(setq-hook! 'c-mode-hook
  tab-width 2
  c-basic-offset tab-width)

(setq-hook! 'c++-mode-hook
  tab-width 2
  c-basic-offset tab-width)

;;; Org

(setq org-directory (file-name-as-directory "~/Documents/Journal")
      org-agenda-files (list org-directory)
      +org-default-todo-file "refile.org"
      +org-default-notes-file "notes.org")

(after! org
  (setq org-clock-out-remove-zero-time-clocks t
        org-display-custom-times t
        org-fast-tag-selection-single-key t
        org-outline-path-complete-in-steps nil
        org-tags-column 0
        org-time-stamp-custom-formats '("[%d-%m-%Y %a]" . "[%d %m %Y  %a [%H:%m]]")
        org-use-fast-todo-selection t

        org-agenda-tags-column org-tags-column

        org-archive-location "archive/%s_archive::"

        org-log-done 'time

        org-refile-use-outline-path 'file
        org-refile-allow-creating-parent-nodes 'confirm
        org-refile-targets '((nil :maxlevel . 1)
                             (org-agenda-files :maxlevel . 1))))

;;; Common Lisp

(defvar lisp-implementations
  `((roswell ("ros" "-Q" "run"))
    (sbcl ("ros" "-L" "sbcl-bin" "run"))
    (ccl ("ros" "-L" "ccl-bin" "run"))))

(setq inferior-lisp-program "ros -Q run")

(after! sly
  (setq sly-lisp-implementations lisp-implementations
        sly-complete-symbol-function 'sly-flex-completions))

;;; Janet

(use-package! janet-mode
  :mode "\\.janet$"
  :hook ((janet-mode . rainbow-delimiters-mode)
         (janet-mode . lispy-mode)))

;;; Zig

(use-package! zig-mode
  :mode "\\.zig$"
  :config
  (setq zig-format-on-save (featurep! :editor format +onsave))
  (map! :map (zig-mode-map)
        :localleader
        "c" #'zig-compile
        "f" #'zig-format-buffer
        "r" #'zig-run
        "t" #'zig-test-buffer
        (:prefix ("b" . "build")
         "e" #'zig-build-exe
         "l" #'zig-build-lib
         "o" #'zig-build-obj)))
